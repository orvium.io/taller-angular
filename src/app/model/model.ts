export interface Restaurant {
  name: string;
  description: string;
  image: string;
  dishes: Dish[];
}

export interface Dish {
  name: string;
  description: string;
  image: string;
  price: number;
}
