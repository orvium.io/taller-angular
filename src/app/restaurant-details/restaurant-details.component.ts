import { NgFor } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Restaurant } from '../model/model';

@Component({
  selector: 'app-restaurant-details',
  standalone: true,
  imports: [MatButtonModule, NgFor],
  templateUrl: './restaurant-details.component.html',
  styleUrls: ['./restaurant-details.component.scss'],
})
export class RestaurantDetailsComponent {
  @Input() restaurant!: Restaurant;
  @Output() closed = new EventEmitter<boolean>();

  closeRestaurant() {
    this.closed.emit(true);
  }
}
