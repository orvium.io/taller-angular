import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';

@Component({
  selector: 'app-toolbar',
  standalone: true,
  imports: [CommonModule, MatIconModule, MatButtonModule, MatToolbarModule],
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent {
  @Input({ required: true }) title = 'Mi Aplicacion de Restaurantes';

  shareApp($event: MouseEvent) {
    alert('Compartir app');
  }

  favoriteApp($event: MouseEvent) {
    alert('App agregada a favoritos');
  }
}
