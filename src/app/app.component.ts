import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { RestaurantCardComponent } from './restaurant-card/restaurant-card.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { NgFor, NgIf } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { SpyDirective } from './spy.directive';
import { RestaurantDetailsComponent } from './restaurant-details/restaurant-details.component';
import { Restaurant } from './model/model';

const defaultRestaurants: Restaurant[] = [
  {
    name: 'Restaurante Elegante',
    description: 'Un restaurante muy elegante',
    image:
      'https://images.unsplash.com/photo-1414235077428-338989a2e8c0?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
    dishes: [
      {
        name: 'Pasta bolognesa',
        description: 'Tradicional pasta bolognesa',
        price: 10,
        image:
          'https://images.unsplash.com/photo-1614777986387-015c2a89b696?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=300&q=80',
      },
      {
        name: 'Pasta al pesto',
        description: 'Tradicional pasta al pesto',
        price: 10,
        image:
          'https://images.unsplash.com/photo-1567608285969-48e4bbe0d399?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=300&q=80',
      },
    ],
  },
  {
    name: 'Extreme Burguers',
    description: 'Las mejores hamburguesas de Vitoria',
    image:
      'https://images.unsplash.com/photo-1466978913421-dad2ebd01d17?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1074&q=80',
    dishes: [
      {
        name: 'Hamburguesa completa',
        description: 'La hamburguesa de toda la vida',
        price: 10,
        image:
          'https://images.unsplash.com/photo-1614891669421-964261109bb4?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=300&q=80',
      },
      {
        name: 'Hamburguesa especial',
        description: 'Hamburguesa en pan con carbon activado',
        price: 10,
        image:
          'https://images.unsplash.com/photo-1666304927476-065fd6a18b21?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=300&q=80',
      },
    ],
  },
];

@Component({
  selector: 'app-root',
  standalone: true,
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  imports: [
    NgFor,
    NgIf,
    RouterOutlet,
    RestaurantCardComponent,
    ToolbarComponent,
    MatButtonModule,
    SpyDirective,
    RestaurantDetailsComponent,
  ],
})
export class AppComponent {
  title = 'Mi aplicacion de restaurantes';
  restaurants: Restaurant[] = [...defaultRestaurants];
  selectedRestaurant?: Restaurant;

  deleteRestaurant() {
    this.restaurants.pop();
  }

  addRestaurant() {
    const newRestaurant = defaultRestaurants[this.restaurants.length % 2];
    this.restaurants.push(newRestaurant);
  }

  selectRestaurant(restaurant: Restaurant) {
    this.selectedRestaurant = restaurant;
  }

  closeRestaurant() {
    this.selectedRestaurant = undefined;
  }
}
