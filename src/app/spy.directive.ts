import {
  AfterViewChecked,
  AfterViewInit,
  Directive,
  DoCheck,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
} from '@angular/core';

let nextId = 1;

@Directive({
  selector: '[appSpy]',
  standalone: true,
})
export class SpyDirective
  implements OnInit, OnDestroy, AfterViewInit, AfterViewChecked, OnChanges, DoCheck
{
  private id = nextId++;

  @Input() spyName = '';

  constructor() {}

  ngOnInit(): void {
    console.log(`Spy ${this.spyName} #${this.id} ngOnInit`);
  }

  ngOnDestroy(): void {
    console.log(`Spy ${this.spyName} #${this.id} ngOnDestroy`);
  }

  ngAfterViewInit(): void {
    console.log(`Spy ${this.spyName} #${this.id} ngAfterViewInit`);
  }

  ngAfterViewChecked(): void {
    console.log(`Spy ${this.spyName} #${this.id} ngAfterViewChecked`);
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(`Spy ${this.spyName} #${this.id} ngOnChanges`);
  }

  ngDoCheck(): void {
    console.log(`Spy ${this.spyName} #${this.id} ngDoCheck`);
  }
}
