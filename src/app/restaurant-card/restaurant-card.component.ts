import { NgIf } from '@angular/common';
import { Component, Input } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { Restaurant } from '../model/model';

@Component({
  selector: 'app-restaurant-card',
  standalone: true,
  imports: [MatCardModule, NgIf],
  templateUrl: './restaurant-card.component.html',
  styleUrls: ['./restaurant-card.component.scss'],
})
export class RestaurantCardComponent {
  @Input({ required: true }) restaurant!: Restaurant;
  @Input() showDescription = true;
}
